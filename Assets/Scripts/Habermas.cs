﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Habermas : MonoBehaviour {

	public AudioClip[] clips;
	AudioSource source;
	public static Habermas theHabermas;
	public Transform[] fromTo;
	public Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		source = GetComponent<AudioSource>();
		theHabermas = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Shoot()
	{
		StopAllCoroutines();
		StartCoroutine(StartShoot());
	}

	IEnumerator StartShoot()
	{
		float time = 0;
		while (time < 2)
		{
			time += Time.deltaTime;
			transform.Translate((fromTo[1].position - transform.position) * time / 2f, Space.World);
			yield return null;
		}
		anim.SetTrigger("Shoot");
		yield return new WaitForSeconds(0.2f);
		source.PlayOneShot(clips[0]);
		yield return new WaitForSeconds(0.3f);
		source.PlayOneShot(clips[1]);
		yield return new WaitForSeconds(0.3f);
		time = 0;
		while (time < 2)
		{
			time += Time.deltaTime;
			transform.Translate((fromTo[0].position - transform.position) * time / 2, Space.World);
			yield return null;
		}
	}
}
