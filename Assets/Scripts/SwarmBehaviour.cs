﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwarmBehaviour : MonoBehaviour {

	[SerializeField]
	SwarmAgent agentPrefab;
	[SerializeField]
	int agentCount;
	[SerializeField]
	float neighborRadius;
	[SerializeField]
	float alignmentWeight;
	[SerializeField]
	float cohesionWeight;
	[SerializeField]
	public float separationWeight;
	[SerializeField]
	public float leadershipWeight;
	[SerializeField]
	float updateInterval;
	[SerializeField]
	float agentBaseSpeed;
	[SerializeField]
	float agentMaxFlapForce;
	float time;

	public static bool swarmReady;



	public SwarmAgent[] agents;

	// Use this for initialization
	void Start()
	{
		StartCoroutine(Setup());
	}

	// Update is called once per frame
	void Update()
	{
		time += Time.deltaTime;
		if (time >= updateInterval)
		{
			time = 0;
			int ag = 0;
			for (int i = 0; i < agents.Length; i++)
			{
				if (agents[i] != null)
				{
					if (agents[i].alive)
					{
						ag++;
						if (!agents[i].leader)
						{
							Vector3 alignment = ComputeAlignment(agents[i]);
							Vector3 cohesion = ComputeCohesion(agents[i]);
							Vector3 separation = ComputeSeparation(agents[i]);
							Vector3 leadership = ComputeLeadership(agents[i]);
							Vector3 v = alignment * alignmentWeight + cohesion * cohesionWeight + separation * separationWeight + leadership * leadershipWeight;
							agents[i].rbody.velocity = v.normalized * agents[i].baseSpeed;
							if (v != Vector3.zero)
							{
								agents[i].model.transform.rotation = Quaternion.LookRotation(v);
							}
							//agents[i].transform.LookAt(agents[0].transform);
						}
						else
						{
							if (PlayerController.playerOneReady && PlayerController.playerTwoReady)
							{
								agents[i].rbody.velocity = new Vector3(0.8f * agents[i].baseSpeed, agents[i].rbody.velocity.y, 0);
							}
						}
					}
				}
			}
			GameManager.agentsLeft = ag;
			if (ag <= 10 && swarmReady)
			{
				Debug.Log("Lost");
				GameManager.newState = gameState.lost;
			}
		}
	}

	//Initializes the Swarm
	IEnumerator Setup()
	{
		agents = new SwarmAgent[agentCount];
		for(int i= 0; i<agentCount; i++)
		{
			agents[i] = Instantiate(agentPrefab, Random.insideUnitCircle*5, Quaternion.identity);
			agents[i].index = i;
			agents[i].baseSpeed = agentBaseSpeed;
			agents[i].flapForce = agentMaxFlapForce;
			agents[i].alive = true;
			yield return new WaitForSeconds(0.005f);
		}
		GameManager.agentsLeft = agentCount;
		agents[0].leader = true;
		agents[0].gameObject.layer = 10;
		agents[0].model.SetActive(false);
		swarmReady = true;
	}

	//Computes Alignment, Cohersion and Separation with weights
	public void UpdateVelocity(SwarmAgent sa)
	{
		Vector3 v = alignmentWeight * ComputeAlignment(sa) + cohesionWeight * ComputeCohesion(sa) + separationWeight * ComputeSeparation(sa);
		sa.rbody.velocity = Vector3.Normalize(v) * sa.baseSpeed;
	}

	public void Flap(SwarmAgent sa)
	{
		sa.rbody.AddForce(Vector3.up * sa.flapForce);
	}


	//Computes the Alignment for an Agent
	public Vector3 ComputeAlignment(SwarmAgent sa)
	{
		Vector3 v = Vector3.zero;
		int neighbors = 0;
		for(int i = 0; i<agents.Length; i++)
		{
			if (agents[i] != null)
			{
				if (agents[i].index != sa.index && agents[i].alive)
				{
					if (Vector2.Distance(sa.transform.position, agents[i].transform.position) < neighborRadius)
					{
						neighbors++;
						v += agents[i].rbody.velocity;
					}
				}
			}
		}
		if (neighbors == 0)
		{
			return v;
		}
		else
		{
			v /= neighbors;
			v.Normalize();
			return v;
		}
	}

	//Computes the Cohesion of an agent
	public Vector3 ComputeCohesion(SwarmAgent sa)
	{
		Vector3 v = Vector3.zero;
		int neighbors = 0;
		for (int i = 0; i < agents.Length; i++)
		{
			if (agents[i] != null)
			{
				if (agents[i].index != sa.index && agents[i].alive)
				{
					if (Vector2.Distance(sa.transform.position, agents[i].transform.position) < neighborRadius)
					{
						neighbors++;
						v += agents[i].transform.position;
					}
				}
			}
		}
		if (neighbors == 0)
		{
			return v;
		}
		else
		{
			v /= neighbors;
			v = v - sa.transform.position;
			v.Normalize();
			return v;
		}
	}

	//Computes the separation of an agent
	public Vector3 ComputeSeparation(SwarmAgent sa)
	{
		Vector3 v = Vector3.zero;
		int neighbors = 0;
		for (int i = 0; i < agents.Length; i++)
		{
			if (agents[i] != null)
			{
				if (agents[i].index != sa.index && agents[i].alive)
				{
					if (Vector2.Distance(sa.transform.position, agents[i].transform.position) < neighborRadius)
					{
						neighbors++;
						v += agents[i].transform.position - sa.transform.position;
					}
				}
			}
		}
		if (neighbors == 0)
		{
			return v;
		}
		else
		{
			v /= neighbors;
			v *= -1;
			v.Normalize();
			return v;
		}
	}

	public Vector3 ComputeLeadership(SwarmAgent sa)
	{
		Vector3 v = Vector3.zero;
		for (int i = 0; i < agents.Length; i++)
		{
			if (agents[i] != null)
			{
				if (agents[i].leader)
				{
					v = agents[i].transform.position - sa.transform.position;
				}
			}
		}
		v.Normalize();
		return v;
	}
}
