﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinZone : MonoBehaviour {

	int arrivedInGoal;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Note"))
		{
			arrivedInGoal++;
			if(arrivedInGoal >= GameManager.agentsLeft)
			{
				GameManager.newState = gameState.won;
			}
		}
	}
}
