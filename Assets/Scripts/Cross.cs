﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cross : MonoBehaviour {
	public Image aim;
	public static Cross theCross;

	// Use this for initialization
	void Start () {
		if (theCross == null)
		{
			theCross = this;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Fade(bool appear, float time, float speed)
	{
		StartCoroutine(Fade(aim, appear, time, speed));
	}

	IEnumerator Fade(Image target, bool appear, float time, float speed)
	{
		float t = 0;
		Color c = target.color;
		while (t < time)
		{
			t += Time.deltaTime;
			yield return null;
			if (appear)
			{
				c.a = Mathf.Lerp(c.a, 1, Time.deltaTime * speed);
				target.color = c;
			}
			else
			{
				c.a = Mathf.Lerp(c.a, 0, Time.deltaTime * speed);
				target.color = c;
			}
		}
		if (appear)
		{
			c.a = 1;
		}
		else
		{
			c.a = 0;
		}
		target.color = c;
	}
}
