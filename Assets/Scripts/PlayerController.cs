﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	[SerializeField]
	SwarmBehaviour swarm;
	public static bool playerOneReady;
	public static bool playerTwoReady;

	public static float maxSeparation;

	public float SeparationResetSpeed;


	// Use this for initialization
	void Start () {
		maxSeparation = swarm.separationWeight;
		playerOneReady = false;
		playerTwoReady = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1"))
		{
			playerOneReady = true;
			swarm.Flap(swarm.agents[0]);
		}

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			GameManager.newState = gameState.menu;
		}

		swarm.separationWeight = Mathf.Lerp(swarm.separationWeight, maxSeparation, Time.deltaTime * SeparationResetSpeed);

		if (Input.GetButtonDown("Fire2"))
		{
			playerTwoReady = true;
			if (swarm.separationWeight > 1)
			{
				swarm.separationWeight -= 0.6f;
			}
		}

		if(!playerOneReady || !playerTwoReady)
		{
			swarm.leadershipWeight = 0;
		}else
		{
			swarm.leadershipWeight = 1;
			swarm.agents[0].rbody.useGravity = true;
		}
	}
}
