﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnitureSpawner : MonoBehaviour {

	[SerializeField]
	Furniture[] furniturePrefabs;

	[SerializeField]
	float minSpawnCD;
	[SerializeField]
	float maxSpawnCD;

	float time;
	float nextSpawn;

	public List<GameObject> curFurniture;

	// Use this for initialization
	void Start () {
		nextSpawn = Random.Range(minSpawnCD, maxSpawnCD);
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		if (time >= nextSpawn)
		{
			time = 0;
			nextSpawn = Random.Range(minSpawnCD, maxSpawnCD);
			SpawnFurniture(Random.Range(0, furniturePrefabs.Length));
		}
	}

	void SpawnFurniture(int index)
	{
		Vector3 spawnPos = new Vector3(Camera.main.transform.position.x + Camera.main.transform.position.z*-2, furniturePrefabs[index].ysize, 0);
		GameObject newFurn = Instantiate(furniturePrefabs[index].gameObject, spawnPos, Quaternion.identity);
		curFurniture.Add(newFurn);
	}
}
