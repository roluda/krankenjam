﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetShot : MonoBehaviour {

	public Vector3 targetPos;
	public Vector3 targetScale;
	public float scaleSpeed;
	public float moveSpeed;
	public float curTime;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		if (transform.position.z <= 0)
		{
			curTime += Time.deltaTime;
			Vector3 curScale = transform.localScale;
			curScale = Vector3.Lerp(curScale, targetScale, curTime / scaleSpeed);
			transform.localScale = curScale;
			targetPos = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, 0.1f);
			transform.Translate((targetPos- transform.position) * (curTime / moveSpeed));
		}
	}
}
