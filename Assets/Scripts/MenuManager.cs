﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Quit();
		}
	}
	public void Play()
	{
		GameManager.newState = gameState.level;
	}

	public void Endless()
	{
		GameManager.level = 3;
		Play();
	}

	public void Quit()
	{
		Application.Quit();
	}
}
