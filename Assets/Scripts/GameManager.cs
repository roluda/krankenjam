﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum gameState { menu, won, lost, level}
public class GameManager : MonoBehaviour {

	public static GameManager theManager;
	public static gameState curState;
	public static gameState newState;
	public static int level = 0;

	public static int agentsLeft;
	public static bool levelActive;

	// Use this for initialization
	void Start () {
		if(FindObjectsOfType<GameManager>().Length > 1)
		{
			Destroy(this);
		}
		else
		{
			DontDestroyOnLoad(this.gameObject);
			theManager = this;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(newState != curState)
		{
			curState = newState;
			OnChangeState(newState);
		}
	}

	public void OnChangeState(gameState state)
	{
		switch (state)
		{
			case gameState.menu:
				level = 0;
				SceneManager.LoadScene(0);
				break;
			case gameState.won:
				level++;
				SceneManager.LoadScene(2);
				break;
			case gameState.lost:
				SceneManager.LoadScene(1);
				break;
			case gameState.level:
				SwarmBehaviour.swarmReady = false;
				SceneManager.LoadScene(level + 3);
				break;
		}
	}
}
