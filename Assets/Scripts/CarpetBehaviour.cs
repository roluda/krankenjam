﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum wallType {bureau, foyer, hallway, random}
public class CarpetBehaviour : MonoBehaviour {
	[SerializeField]
	wallType wallType;

	[SerializeField]
	GameObject[] wallStartPrefab;
	[SerializeField]
	GameObject[] wallSegmentPrefab;
	[SerializeField]
	int repeats;
	// Use this for initialization
	void Start () {
		if(wallType == wallType.random)
		{
			wallType = (wallType)Random.Range(0, wallStartPrefab.Length);
		}
		float posX = 0;
		Vector3 pos = new Vector3(posX, 0, 1);
		Instantiate(GetWallSegment(wallStartPrefab), pos, Quaternion.identity);
		for (int i = 2; i < repeats +2; i++)
		{
			posX = i * 15f;
			pos.x = posX;
			GameObject newWall = Instantiate(GetWallSegment(wallSegmentPrefab), pos, Quaternion.identity);
			newWall.GetComponent<SpriteRenderer>().sortingOrder = -i;
		}
	}

	GameObject GetWallSegment(GameObject[] template)
	{
		switch (wallType)
		{
			case wallType.bureau:
				return template[0];
			case wallType.foyer:
				return template[1];
			case wallType.hallway:
				return template[2];
			case wallType.random:
				return template[0];
			default:
				return template[0];
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
