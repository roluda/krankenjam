﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WinText : MonoBehaviour {

	public TextMeshProUGUI saved;

	// Use this for initialization
	void Start () {
		saved.text = "You saved " + GameManager.agentsLeft + " of 70 thoughts.";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
