﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour {

	[SerializeField]
	SwarmBehaviour swarm;

	public float smoothTimeX;
	public float smoothTimeY;
	

	Vector3 velocity;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (SwarmBehaviour.swarmReady)
		{
			Vector3 newPos = new Vector3();
			int agentsAlive = 0;
			for (int i = 0; i < swarm.agents.Length; i++)
			{
				if (swarm.agents[i].alive)
				{
					agentsAlive++;
					newPos += swarm.agents[i].transform.position;
				}
			}
			newPos /= agentsAlive;
			float posX = Mathf.SmoothDamp(transform.position.x, newPos.x, ref velocity.x, smoothTimeX);
			float posY = Mathf.SmoothDamp(transform.position.y, newPos.y, ref velocity.y, smoothTimeY);
			transform.position = new Vector3(posX, posY, -20);
		}
	}
}
