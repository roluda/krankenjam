﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sniper : MonoBehaviour {

	bool triggered = false;

	[SerializeField]
	GameObject hitBox;

	public NetShot netShotPrefab;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Note") && !triggered)
		{
			triggered = true;
			StartCoroutine(Snipe(3));
		}
	}

	IEnumerator Snipe(float time)
	{
		Cross.theCross.Fade(true, 0.5f, 2);
		Habermas.theHabermas.Shoot();
		yield return new WaitForSeconds(time-.25f);
		Instantiate(netShotPrefab, NetShotter.origin, Quaternion.identity);
		yield return new WaitForSeconds(.25f);
		Vector3 pos = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, 0);
		GameObject hb = Instantiate(hitBox, pos, Quaternion.identity);
		yield return new WaitForFixedUpdate();
		yield return new WaitForFixedUpdate();
		Cross.theCross.Fade(false, 0.5f, 10);
		Destroy(hb);
	}

	
}
