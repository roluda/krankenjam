﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

enum paperState { flattern, flatternIntense, gleiten}
public class SoundBehaviour : MonoBehaviour {

	paperState pstate = paperState.flattern;
	public AudioMixerSnapshot flattern;
	public AudioMixerSnapshot flatternIntense;
	public AudioMixerSnapshot gleiten;

	public SwarmBehaviour swarm;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(swarm.separationWeight < 2)
		{
			if(pstate != paperState.gleiten)
			{
				ChangeSnapshot(paperState.gleiten);
			}
		}else if(swarm.separationWeight > PlayerController.maxSeparation - 2)
		{
			if(pstate != paperState.flatternIntense)
			{
				ChangeSnapshot(paperState.flatternIntense);
			}
		}
		else
		{
			if(pstate != paperState.flattern)
			{
				ChangeSnapshot(paperState.flattern);
			}
		}
	}

	void ChangeSnapshot(paperState state)
	{
		pstate = state;
		switch (state)
		{
			case paperState.gleiten:
				gleiten.TransitionTo(1f);
				break;
			case paperState.flattern:
				flattern.TransitionTo(0.1f);
				break;
			case paperState.flatternIntense:
				flatternIntense.TransitionTo(0.1f);
				break;
		}
	}
}
