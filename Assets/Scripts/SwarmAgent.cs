﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwarmAgent : MonoBehaviour {

	public Animator anim;
	public GameObject modelPrefab;
	public GameObject model;
	public Rigidbody rbody;
	public Collider col;
	public int index;
	public float baseSpeed;
	public float flapForce;
	public bool leader;
	public bool alive;

	private void Awake()
	{
		rbody = GetComponent<Rigidbody>();
		col = GetComponent<Collider>();
	}

	// Use this for initialization
	void Start () {
		model = Instantiate(modelPrefab, transform);
		anim = model.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!leader)
		{
			anim.SetFloat("VelocityY", rbody.velocity.y);
		}
	}

	void SniperDeath()
	{
		if (!leader)
		{
			gameObject.layer = 13;
			alive = false;
			rbody.useGravity = false;
			rbody.velocity = Vector3.zero;
			transform.position = new Vector3(transform.position.x, transform.position.y, .3f);
			transform.rotation = Quaternion.Euler(-180,0,0);
			anim.SetFloat("Blend", 0);
		}
	}

	void FurnitureDeath()
	{
		anim.SetFloat("Blend", 0);
		alive = false;
		gameObject.layer = 13;
		rbody.useGravity = true;
		rbody.freezeRotation = false;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("DeathBox"))
		{
			SniperDeath();
		}
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.GetComponent<Collider>().CompareTag("Furniture"))
		{
			FurnitureDeath();
		}
	}
}
