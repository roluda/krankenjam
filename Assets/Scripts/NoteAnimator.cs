﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteAnimator : MonoBehaviour {

	Animator anim;
	public Rigidbody rigid;

	// Use this for initialization
	void Awake () {
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		anim.SetFloat("VelocityY", rigid.velocity.y);
	}
}
